import React from 'react'

const PersonRow=(props)=>{
    // const{no,person}=props;

   const no =props.no;
   const person=props.person;


    return(
        <tr>
<th scope="row">{no}</th>
    <td>{person.name}</td>
<td>{person.weight}</td>
        </tr>
    )
}
export default PersonRow;